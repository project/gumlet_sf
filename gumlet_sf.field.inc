<?php

/**
 * @file
 * Implement an image field, based on the file module's file field.
 */

/**
 * Prepares variables for Gumlet image formatter templates.
 *
 * Default template: gumlet-sf-formatter.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - item: An ImageItem object.
 *   - item_attributes: An optional associative array of html attributes to be
 *     placed in the img tag.
 *   - image_style: An optional image style.
 *   - url: An optional \Drupal\Core\Url object.
 */
function template_preprocess_gumlet_sf_formatter(&$variables)
{

    $variables['image']['attributes'] = $variables['item_attributes'];
    $variables['image']['lazyload'] = $variables['lazyload'];
    $variables['image']['placeholder'] = $variables['placeholder'];

    $item = $variables['item'];
    $image_style_data = [];
    $image_style_dimensions = [];
    $gumlet_sf_style_id = $variables['gumlet_sf_style_id'];
    if (isset($gumlet_sf_style_id)) {
        $image_style_data = gumlet_sf_load($gumlet_sf_style_id);
    }
    if (isset($image_style_data['width'])) {
        $image_style_dimensions['width'] = $image_style_data['width'];
    }
    if (isset($image_style_data['height'])) {
          $image_style_dimensions['height'] = $image_style_data['height'];
    }
    if (isset($image_style_data['crop']) && $image_style_data['crop'] == 'focalpoint') {
        if (isset($variables['gumlet_fp_x']) && !empty($variables['gumlet_fp_x'])) {
            if (!empty($item->width)) {
                $width = $item->width;
            } else {
                $width = $image_style_dimensions['width'];
            }
            $image_style_data['fp-x'] = round($variables['gumlet_fp_x'] / $width, 2);
        }
        if (isset($variables['gumlet_fp_y']) && !empty($variables['gumlet_fp_y'])) {
            if (!empty($item->height)) {
                $height = $item->height;
            } else {
                $height = $image_style_dimensions['height'];
            }
            $image_style_data['fp-y'] = round($variables['gumlet_fp_y'] / $height, 2);
        }
    }
    // Do not output an empty 'title' attribute.
    if (mb_strlen($item->title) != 0) {
        $variables['image']['title'] = $item->title;
    }
    $urlImage = '';
    if (($entity = $item->entity) && empty($item->uri)) {
        $image_uri = $entity->getFileUri();
        $url = $entity->createFileUrl(false);
        $urlImage = \Drupal::service('file_url_generator')->transformRelative($url);
    } else {
        $image_uri = $item->uri;
        $url = $entity->createFileUrl(false);
        $urlImage = \Drupal::service('file_url_generator')->transformRelative($url);
    }
    if (isset($image_style_data) && !empty($image_style_data)) {
        $urlImage .= gumlet_sf_formatter($image_style_data);
    }
    $variables['image']['url'] = $urlImage;

    foreach (['width', 'height', 'alt'] as $key) {
        if ($key == 'width' && isset($image_style_dimensions['width']) && !empty(isset($image_style_dimensions['width']))) {
            $item->$key =  $image_style_dimensions['width'];
        }
        if ($key == 'height' && isset($image_style_dimensions['height']) && !empty(isset($image_style_dimensions['height']))) {
            $item->$key =  $image_style_dimensions['height'];
        }
        $variables['image']["$key"] = $item->$key;
    }
}
