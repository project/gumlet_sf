<?php

namespace Drupal\gumlet_sf\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

class GumletSFEditForm extends FormBase
{
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'gumlet_sf_edit_form';
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $id = "")
    {
        $gumletSf = gumlet_sf_modes();
        $ajaxWrapper = 'filter-ajax-wrapper';
        $values = $form_state->getValues();

        $gumletStyles = \Drupal::database()->select('gumlet_sf', 'gs');
        $gumletStyles->fields('gs');
        $gumletStyles->condition('gs.id', $id);
        $gumletStyles->range('0', '1');
        $gumletStyles->orderBy('gs.id', 'DESC');
        $gumletStylesResults =  $gumletStyles->execute()->fetchObject();
        $dataObject = [];
        if (!empty($gumletStylesResults) && empty($values)) {
            $values['style_mode'] = $gumletStylesResults->mode;
            $dataObject = unserialize($gumletStylesResults->data);
        }
        $form['name'] = [
          '#type' => 'textfield',
          '#required' => true,
          '#title' => 'Gumlet Style Name',
          '#default_value' =>  $gumletStylesResults->name ? $gumletStylesResults->name : '',
        ];

        $form['style_mode'] = [
          '#type' => 'select',
          '#title' => 'Gumlet Style Mode',
          '#required' => true,
          '#empty_option' => '- Select a value -',
          '#default_value' =>  $gumletStylesResults->mode ? $gumletStylesResults->mode : '',
          '#options' => $gumletSf,
          '#ajax' => [
            'callback' => [$this, 'gumletSFChange'],
            'event' => 'change',
            'wrapper' => $ajaxWrapper,
          ],
        ];

        $form['mode_extensions'] = [
          '#type' => 'container',
          '#attributes' => [
            'id' => $ajaxWrapper,
          ]
        ];
        $form['mode_extensions']['id']  = array(
            '#type'          => 'hidden',
            '#value'         => $id,
          );
        //
        if (isset($values['style_mode'])) {
            $form['mode_extensions'][$values['style_mode']] = array(
              '#type'          => 'fieldset',
              '#title'         => $values['style_mode'] . ' ' . t('Mode'),
            );
            $form['mode_extensions'][$values['style_mode']]['width']  = array(
              '#type'          => 'number',
              '#title'         => t('Width'),
              '#default_value' =>  (isset($dataObject['width'])) ? $dataObject['width'] : '',
            );
            $form['mode_extensions'][$values['style_mode']]['height']  = array(
              '#type'          => 'number',
              '#title'         => t('Height'),
              '#default_value' =>  (isset($dataObject['height'])) ? $dataObject['height'] : '',
            );
            if ($values['style_mode'] != 'mask' || $values['style_mode'] != 'enlarge' || $values['style_mode'] != 'dpr') {
                $form['mode_extensions'][$values['style_mode']]['mode']  = array(
                  '#type'          => 'hidden',
                  '#value'         => $values['style_mode'],
                );
            }
            if ($values['style_mode'] == 'dpr') {
                $form['mode_extensions']['dpr']['dpr_value']  = array(
                  '#type'          => 'select',
                  '#title'         => t('DPR'),
                  '#empty_option' => '- Select a value -',
                  '#options'        =>  gumlet_sf_float(),
                  '#default_value' =>  (isset($dataObject['dpr'])) ? $dataObject['dpr'] : '',
                );
            }
            if ($values['style_mode'] == 'mask') {
                $form['mode_extensions']['mask']['mask_value']  = array(
                  '#type'          => 'hidden',
                  '#value'         => 'ellipse',
                );
            }
            if ($values['style_mode'] == 'enlarge') {
                $form['mode_extensions']['enlarge']['enlarge_value']  = array(
                  '#type'          => 'hidden',
                  '#value'         => 'true',
                );
            }
            if ($values['style_mode'] == 'fill') {
                $form['mode_extensions']['fill']['fill-type']  = array(
                  '#type'          => 'select',
                  '#title'         => t('Fill'),
                  '#empty_option' => '- Select a value -',
                  '#options' => ['solid' => 'Solid', 'blur' => 'Blur'],
                  '#default_value' =>  (isset($dataObject['fill'])) ? $dataObject['fill'] : '',
                );
                $form['mode_extensions']['fill']['fill-color']  = array(
                  '#type'          => 'textfield',
                  '#title'         => t('Fill color'),
                  '#default_value' =>  (isset($dataObject['color'])) ? $dataObject['color'] : '',
                  '#states'        => array(
                    'enabled'      => array(
                      ':input[name="fill-type"]' => array('value' => 'solid'),
                    ),
                    'visible'      => array(
                      ':input[name="fill-type"]' => array('value' => 'solid'),
                    ),
                  ),
                );
            }
            if ($values['style_mode'] == 'extract') {
                $extractdefault = [];
                if (isset($dataObject['extract'])) {
                    $extractdefault = explode(',', $dataObject['extract']);
                }
                $form['mode_extensions'][$values['style_mode']]['left']  = array(
                  '#type'          => 'number',
                  '#title'         => t('Extract Left'),
                '#default_value' =>  (isset($extractdefault[0])) ? $extractdefault[0] : '',
                );
                $form['mode_extensions'][$values['style_mode']]['top']  = array(
                  '#type'          => 'number',
                  '#title'         => t('Extract Top'),
                '#default_value' =>  (isset($extractdefault[1])) ? $extractdefault[1] : '',
                );
                $form['mode_extensions'][$values['style_mode']]['extract_width']  = array(
                  '#type'          => 'number',
                  '#title'         => t('Extract Width'),
                '#default_value' =>  (isset($extractdefault[2])) ? $extractdefault[2] : '',
                );
                $form['mode_extensions'][$values['style_mode']]['extract_height']  = array(
                  '#type'          => 'number',
                  '#title'         => t('Extract Height'),
                '#default_value' =>  (isset($extractdefault[3])) ? $extractdefault[3] : '',
                );
            }
            if ($values['style_mode'] == 'pad') {
                $paddefault = [];
                if (isset($dataObject['pad'])) {
                    $paddefault = explode(',', $dataObject['pad']);
                }
                $form['mode_extensions'][$values['style_mode']]['padtop']  = array(
                  '#type'          => 'number',
                  '#title'         => t('Pad Left'),
                '#default_value' =>  (isset($paddefault[0])) ? $paddefault[0] : '',
                );
                $form['mode_extensions'][$values['style_mode']]['padright']  = array(
                  '#type'          => 'number',
                  '#title'         => t('Pad Top'),
                '#default_value' =>  (isset($paddefault[1])) ? $paddefault[1] : '',
                );
                $form['mode_extensions'][$values['style_mode']]['padbottom']  = array(
                  '#type'          => 'number',
                  '#title'         => t('Pad Bottom'),
                '#default_value' =>  (isset($paddefault[2])) ? $paddefault[2] : '',
                );
                $form['mode_extensions'][$values['style_mode']]['padleft']  = array(
                  '#type'          => 'number',
                  '#title'         => t('Pad Left'),
                '#default_value' =>  (isset($paddefault[3])) ? $paddefault[3] : '',
                );
                $form['mode_extensions'][$values['style_mode']]['bg']  = array(
                  '#type'          => 'textfield',
                  '#title'         => t('Pad Color Code'),
                '#default_value' =>  (isset($values['bg'])) ? $values['bg'] : '',
                );
            }
            if ($values['style_mode'] == 'crop') {
                $form['mode_extensions'][$values['style_mode']]['crop-type']  = array(
                  '#type'          => 'select',
                  '#title'         => t('Crop Type'),
                  '#required'         => true,
                  '#default_value' =>  (isset($dataObject['crop'])) ? $dataObject['crop'] : '',
                  '#empty_option' => '- Select a value -',
                  '#options' => [
                    'entropy' => 'Entropy',
                    'smart' => 'Smart',
                    'top' => 'Top',
                    'topleft' => 'Top left',
                    'topright' => 'Top Right',
                    'left' => 'Left',
                    'bottomleft' => 'Bottomleft',
                    'bottomright' => 'Bottomright',
                    'bottom' => 'Bottom',
                    'right' => 'Right',
                    'faces' => 'Faces',
                    'center' => 'Center',
                    'focalpoint' => 'Focalpoint'
                  ],
                );
                $form['mode_extensions'][$values['style_mode']]['fp-y']  = array(
                  '#type'          => 'select',
                  '#title'         => t('Focalpoint Y'),
                  '#default_value' =>  (isset($dataObject['fp-y'])) ? $dataObject['fp-y'] : '',
                  '#empty_option' => '- Select a value -',
                  '#options'        =>  gumlet_sf_float(),
                  '#description'  => t('fp-y must be float value between 0.0 to 1.0'),
                  '#states'        => array(
                    'visible'      => array(
                      ':input[name="crop-type"]' => array('value' => 'focalpoint'),
                    ),
                  ),
                );
                $form['mode_extensions'][$values['style_mode']]['fp-x']  = array(
                  '#type'          => 'select',
                  '#title'         => t('Focalpoint X'),
                  '#default_value' =>  (isset($dataObject['fp-x'])) ? $dataObject['fp-x'] : '',
                  '#empty_option' => '- Select a value -',
                  '#options'        =>  gumlet_sf_float(),
                  '#description'  => t('fp-x must be float value between 0.0 to 1.0'),
                  '#states'        => array(
                    'visible'      => array(
                      ':input[name="crop-type"]' => array('value' => 'focalpoint'),
                    ),
                  ),
                );
                $form['mode_extensions'][$values['style_mode']]['fp-z']  = array(
                  '#type'          => 'select',
                  '#title'         => t('Focalpoint Zoom'),
                  '#default_value' =>  (isset($dataObject['fp-z'])) ? $dataObject['fp-z'] : '',
                  '#empty_option' => '- Select a value -',
                  '#options'        =>  gumlet_sf_numbers(),
                  '#description'  => t('fp-z can be in the range of 1 to 10 and default value is 1'),
                  '#states'        => array(
                    'visible'      => array(
                      ':input[name="crop-type"]' => array('value' => 'focalpoint'),
                    ),
                  ),
                );

                $form['mode_extensions'][$values['style_mode']]['ar']  = array(
                  '#type'          => 'select',
                  '#title'         => t('Aspect ratio (ar)'),
                  '#default_value' =>  (isset($dataObject['ar'])) ? $dataObject['ar'] : '',
                  '#empty_option' => '- Select a value -',
                  '#options'        =>  gumlet_sf_ratios(),
                  '#description'  => t('E.g. ar=3:4&mode=crop will crop image in aspect ratio of 3:4.'),
                );
                $form['mode_extensions'][$values['style_mode']]['mode']  = array(
                  '#type'          => 'hidden',
                  '#value'         => $values['style_mode']
                );
            }
        }

        $form['submit'] = array(
          '#type' => 'submit',
          '#value' => $this->t('Update'),
        );
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function gumletSFChange(array $form, FormStateInterface $form_state)
    {
      // Return the element that will replace the wrapper (we return itself).
        return $form['mode_extensions'];
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
        $submissionArry = [];
        if (!empty($values['width'])) {
            $submissionArry['width'] = $values['width'];
        }
        if (!empty($values['height'])) {
            $submissionArry['height'] = $values['height'];
        }
        if ($values['style_mode'] == 'dpr') {
            if (!empty($values['dpr_value'])) {
                $submissionArry['dpr'] = $values['dpr_value'];
            }
        }
        if ($values['style_mode'] == 'pad') {
            if (!empty($values['padtop'])) {
                $submissionArry['pad'] = $values['padtop'] . ',';
            } else {
                $submissionArry['pad'] = '0,';
            }
            if (!empty($values['padright'])) {
                  $submissionArry['pad'] .= $values['padright'] . ',';
            } else {
                  $submissionArry['pad'] .= '0,';
            }
            if (!empty($values['padbottom'])) {
                  $submissionArry['pad'] .= $values['padbottom'] . ',';
            } else {
                  $submissionArry['pad'] .= '0,';
            }
            if (!empty($values['padleft'])) {
                  $submissionArry['pad'] .= $values['padleft'];
            } else {
                  $submissionArry['pad'] .= '0';
            }
            if (!empty($values['bg'])) {
                  $submissionArry['bg'] = $values['bg'];
            }
        }
        if ($values['style_mode'] == 'extract') {
            if (!empty($values['left'])) {
                  $submissionArry['extract'] = $values['left'] . ',';
            } else {
                  $submissionArry['extract'] = '0,';
            }
            if (!empty($values['top'])) {
                  $submissionArry['extract'] .= $values['top'] . ',';
            } else {
                  $submissionArry['extract'] .= '0,';
            }
            if (!empty($values['extract_width'])) {
                  $submissionArry['extract'] .= $values['extract_width'] . ',';
            } else {
                  $submissionArry['extract'] .= '0,';
            }
            if (!empty($values['extract_height'])) {
                  $submissionArry['extract'] .= $values['extract_height'];
            } else {
                  $submissionArry['extract'] .= '0';
            }
        }
        if ($values['style_mode'] == 'crop') {
            if (!empty($values['crop-type'])) {
                  $submissionArry['crop'] = $values['crop-type'];
            }
            if (!empty($values['ar'])) {
                  $submissionArry['ar'] = $values['ar'];
            }
            if (!empty($values['fp-y'])) {
                  $submissionArry['fp-y'] = $values['fp-y'];
            }
            if (!empty($values['fp-x'])) {
                  $submissionArry['fp-x'] = $values['fp-x'];
            }
            if (!empty($values['fp-z'])) {
                  $submissionArry['fp-z'] = $values['fp-z'];
            }
        }
        if ($values['style_mode'] == 'mask') {
            if (!empty($values['mask_value'])) {
                  $submissionArry['mask'] = $values['mask_value'];
            }
        }
        if ($values['style_mode'] == 'fill') {
            if (!empty($values['fill-type'])) {
                  $submissionArry['fill'] = $values['fill-type'];
            }
            if (!empty($values['fill-color'])) {
                  $submissionArry['color'] = $values['fill-color'];
            }
        }
        if ($values['style_mode'] == 'enlarge') {
            $submissionArry['enlarge'] = $values['enlarge_value'];
        }
        if ($values['style_mode'] != 'enlarge' && $values['style_mode'] != 'mask' && $values['style_mode'] != 'extract' && $values['style_mode'] != 'pad' && $values['style_mode'] != 'dpr') {
            $submissionArry['mode'] = $values['mode'];
        }

        $submissionserialize = serialize($submissionArry);

          $query = \Drupal::database();
          $query->update('gumlet_sf')
          ->fields(['name' => $values['name'], 'mode' => $values['mode'], 'data' => $submissionserialize])
          ->condition('id', $values['id'])
          ->execute();
        \Drupal::messenger()->addMessage(t('Gumlet Image Styles has been updated Successfully.'), 'status');
        $redirect = Url::fromRoute('gumlet_sf.styles')->setAbsolute()->toString();
        $response = new RedirectResponse($redirect);
        $response->send();
        return;
    }
}
