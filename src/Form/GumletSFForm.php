<?php

namespace Drupal\gumlet_sf\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

class GumletSFForm extends FormBase
{
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'gumlet_sf_add_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $gumletSf = gumlet_sf_modes();
        $ajaxWrapper = 'filter-ajax-wrapper';
        $values = $form_state->getValues();
        $form['name'] = [
          '#type' => 'textfield',
          '#required' => true,
          '#title' => 'Gumlet Style Name',
        ];

        $form['style_mode'] = [
          '#type' => 'select',
          '#title' => 'Gumlet Style Mode',
          '#empty_option' => '- Select a value -',
          '#required' => true,
          '#options' => $gumletSf,
          '#ajax' => [
            'callback' => [$this, 'gumletSFChange'],
            'event' => 'change',
            'wrapper' => $ajaxWrapper,
          ],
        ];

        $form['mode_extensions'] = [
          '#type' => 'container',
          '#attributes' => [
            'id' => $ajaxWrapper,
          ]
        ];
        //
        if (isset($values['style_mode'])) {
            $form['mode_extensions'][$values['style_mode']] = array(
              '#type'          => 'fieldset',
              '#title'         => $values['style_mode'] . ' ' . t('Mode'),
            );
            $form['mode_extensions'][$values['style_mode']]['width']  = array(
              '#type'          => 'number',
              '#title'         => t('Width'),
            );
            $form['mode_extensions'][$values['style_mode']]['height']  = array(
              '#type'          => 'number',
              '#title'         => t('Height'),
            );
            if ($values['style_mode'] != 'mask' || $values['style_mode'] != 'enlarge' || $values['style_mode'] != 'dpr') {
                $form['mode_extensions'][$values['style_mode']]['mode']  = array(
                  '#type'          => 'hidden',
                  '#value'         => $values['style_mode'],
                );
            }
            if ($values['style_mode'] == 'dpr') {
                $form['mode_extensions']['dpr']['dpr_value']  = array(
                  '#type'          => 'select',
                  '#empty_option' => '- Select a value -',
                  '#options'        =>  gumlet_sf_float(),
                  '#title'         => t('DPR'),
                );
            }
            if ($values['style_mode'] == 'mask') {
                $form['mode_extensions']['mask']['mask_value']  = array(
                  '#type'          => 'hidden',
                  '#value'         => 'ellipse',
                );
            }
            if ($values['style_mode'] == 'enlarge') {
                $form['mode_extensions']['enlarge']['enlarge_value']  = array(
                  '#type'          => 'hidden',
                  '#value'         => 'true',
                );
            }
            if ($values['style_mode'] == 'fill') {
                $form['mode_extensions']['fill']['fill-type']  = array(
                  '#type'          => 'select',
                  '#title'         => t('Fill'),
                  '#empty_option' => '- Select a value -',
                  '#options' => ['solid' => 'Solid', 'blur' => 'Blur'],
                );
                $form['mode_extensions']['fill']['fill-color']  = array(
                  '#type'          => 'textfield',
                  '#title'         => t('Fill color'),
                  '#states'        => array(
                    'enabled'      => array(
                      ':input[name="fill-type"]' => array('value' => 'solid'),
                    ),
                    'visible'      => array(
                      ':input[name="fill-type"]' => array('value' => 'solid'),
                    ),
                  ),
                );
            }
            if ($values['style_mode'] == 'extract') {
                $form['mode_extensions'][$values['style_mode']]['left']  = array(
                  '#type'          => 'number',
                  '#title'         => t('Extract Left'),
                );
                $form['mode_extensions'][$values['style_mode']]['top']  = array(
                  '#type'          => 'number',
                  '#title'         => t('Extract Top'),
                );
                $form['mode_extensions'][$values['style_mode']]['extract_width']  = array(
                  '#type'          => 'number',
                  '#title'         => t('Extract Width'),
                );
                $form['mode_extensions'][$values['style_mode']]['extract_height']  = array(
                  '#type'          => 'number',
                  '#title'         => t('Extract Height'),
                );
            }
            if ($values['style_mode'] == 'pad') {
                $form['mode_extensions'][$values['style_mode']]['padtop']  = array(
                  '#type'          => 'number',
                  '#title'         => t('Pad Top'),
                );
                $form['mode_extensions'][$values['style_mode']]['padright']  = array(
                  '#type'          => 'number',
                  '#title'         => t('Pad Right'),
                );
                $form['mode_extensions'][$values['style_mode']]['padbottom']  = array(
                  '#type'          => 'number',
                  '#title'         => t('Pad Bottom'),
                );
                $form['mode_extensions'][$values['style_mode']]['padleft']  = array(
                  '#type'          => 'number',
                  '#title'         => t('Pad Left'),
                );
                $form['mode_extensions'][$values['style_mode']]['bg']  = array(
                  '#type'          => 'textfield',
                  '#title'         => t('Pad Color Code'),
                );
            }
            if ($values['style_mode'] == 'crop') {
                $form['mode_extensions'][$values['style_mode']]['crop-type']  = array(
                  '#type'          => 'select',
                  '#title'         => t('Crop Type'),
                  '#required'         => true,
                  '#empty_option' => '- Select a value -',
                  '#options' => [
                    'entropy' => 'Entropy',
                    'smart' => 'Smart',
                    'top' => 'Top',
                    'topleft' => 'Top left',
                    'topright' => 'Top Right',
                    'left' => 'Left',
                    'bottomleft' => 'Bottomleft',
                    'bottomright' => 'Bottomright',
                    'bottom' => 'Bottom',
                    'right' => 'Right',
                    'faces' => 'Faces',
                    'center' => 'Center',
                    'focalpoint' => 'Focalpoint'
                  ],
                );
                $form['mode_extensions'][$values['style_mode']]['fp-y']  = array(
                  '#type'          => 'select',
                  '#title'         => t('Focalpoint Y'),
                  '#default_value' =>  '',
                  '#empty_option' => '- Select a value -',
                  '#options'        =>  gumlet_sf_float(),
                  '#description'  => t('fp-y must be float value between 0.0 to 1.0'),
                  '#states'        => array(
                  'visible'      => array(
                    ':input[name="crop-type"]' => array('value' => 'focalpoint'),
                  ),
                ),
                );
                $form['mode_extensions'][$values['style_mode']]['fp-x']  = array(
                  '#type'          => 'select',
                  '#title'         => t('Focalpoint X'),
                  '#default_value' =>  '',
                  '#empty_option' => '- Select a value -',
                  '#options'        =>  gumlet_sf_float(),
                  '#description'  => t('fp-x must be float value between 0.0 to 1.0'),
                  '#states'        => array(
                  'visible'      => array(
                    ':input[name="crop-type"]' => array('value' => 'focalpoint'),
                  ),
                ),
                );
                $form['mode_extensions'][$values['style_mode']]['fp-z']  = array(
                  '#type'          => 'select',
                  '#title'         => t('Focalpoint Zoom'),
                  '#default_value' =>  '',
                  '#empty_option' => '- Select a value -',
                  '#options'        =>  gumlet_sf_numbers(),
                  '#description'  => t('fp-z can be in the range of 1 to 10 and default value is 1'),
                  '#states'        => array(
                    'visible'      => array(
                      ':input[name="crop-type"]' => array('value' => 'focalpoint'),
                    ),
                  ),
                );

                $form['mode_extensions'][$values['style_mode']]['ar']  = array(
                  '#type'          => 'select',
                  '#title'         => t('Aspect ratio (ar)'),
                  '#empty_option' => '- Select a value -',
                  '#options'        =>  gumlet_sf_ratios(),
                  '#description'  => t('E.g. ar=3:4&mode=crop will crop image in aspect ratio of 3:4.'),
                );
                $form['mode_extensions'][$values['style_mode']]['mode']  = array(
                  '#type'          => 'hidden',
                  '#value'         => $values['style_mode']
                );
            }
        }

        $form['submit'] = array(
          '#type' => 'submit',
          '#value' => $this->t('Create'),
        );
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function gumletSFChange(array $form, FormStateInterface $form_state)
    {
      // Return the element that will replace the wrapper (we return itself).
        return $form['mode_extensions'];
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
        $submissionGumletSF = [];
        if (!empty($values['width'])) {
              $submissionGumletSF['width'] = $values['width'];
        }
        if (!empty($values['height'])) {
              $submissionGumletSF['height'] = $values['height'];
        }

        if ($values['style_mode'] == 'dpr') {
            if (!empty($values['dpr_value'])) {
                  $submissionGumletSF['dpr'] = $values['dpr_value'];
            }
        }
        if ($values['style_mode'] == 'pad') {
            if (!empty($values['padtop'])) {
                  $submissionGumletSF['pad'] = $values['padtop'] . ',';
            } else {
                  $submissionGumletSF['pad'] = '0,';
            }
            if (!empty($values['padright'])) {
                  $submissionGumletSF['pad'] .= $values['padright'] . ',';
            } else {
                  $submissionGumletSF['pad'] .= '0,';
            }
            if (!empty($values['padbottom'])) {
                  $submissionGumletSF['pad'] .= $values['padbottom'] . ',';
            } else {
                  $submissionGumletSF['pad'] .= '0,';
            }
            if (!empty($values['padleft'])) {
                  $submissionGumletSF['pad'] .= $values['padleft'];
            } else {
                  $submissionGumletSF['pad'] .= '0';
            }
            if (!empty($values['bg'])) {
                  $submissionGumletSF['bg'] = $values['bg'];
            }
        }
        if ($values['style_mode'] == 'extract') {
            if (!empty($values['left'])) {
                  $submissionGumletSF['extract'] = $values['left'] . ',';
            } else {
                  $submissionGumletSF['extract'] = '0,';
            }
            if (!empty($values['top'])) {
                  $submissionGumletSF['extract'] .= $values['top'] . ',';
            } else {
                  $submissionGumletSF['extract'] .= '0,';
            }
            if (!empty($values['extract_width'])) {
                  $submissionGumletSF['extract'] .= $values['extract_width'] . ',';
            } else {
                  $submissionGumletSF['extract'] .= '0,';
            }
            if (!empty($values['extract_height'])) {
                  $submissionGumletSF['extract'] .= $values['extract_height'];
            } else {
                  $submissionGumletSF['extract'] .= '0';
            }
        }
        if ($values['style_mode'] == 'crop') {
            if (!empty($values['crop-type'])) {
                  $submissionGumletSF['crop'] = $values['crop-type'];
            }
            if (!empty($values['ar'])) {
                  $submissionGumletSF['ar'] = $values['ar'];
            }
            if (!empty($values['fp-y'])) {
                  $submissionGumletSF['fp-y'] = $values['fp-y'];
            }
            if (!empty($values['fp-x'])) {
                  $submissionGumletSF['fp-x'] = $values['fp-x'];
            }
            if (!empty($values['fp-z'])) {
                  $submissionGumletSF['fp-z'] = $values['fp-z'];
            }
        }
        if ($values['style_mode'] == 'mask') {
            if (!empty($values['mask_value'])) {
                $submissionGumletSF['mask'] = $values['mask_value'];
            }
        }
        if ($values['style_mode'] == 'fill') {
            if (!empty($values['fill-type'])) {
                $submissionGumletSF['fill'] = $values['fill-type'];
            }
            if (!empty($values['fill-color'])) {
                $submissionGumletSF['color'] = $values['fill-color'];
            }
        }
        if ($values['style_mode'] == 'enlarge') {
            $submissionGumletSF['enlarge'] = $values['enlarge_value'];
        }
        if ($values['style_mode'] != 'enlarge' && $values['style_mode'] != 'mask' && $values['style_mode'] != 'extract' && $values['style_mode'] != 'pad' && $values['style_mode'] != 'dpr') {
            $submissionGumletSF['mode'] = $values['mode'];
        }

        $submissionSerialize = serialize($submissionGumletSF);
        \Drupal::database()->insert('gumlet_sf')
        ->fields([
          'mode' => $values['style_mode'],
          'name' => $values['name'],
          'data' => $submissionSerialize,
        ])
        ->execute();
        \Drupal::messenger()->addMessage(t('Gumlet Image Styles has been created Successfully.'), 'status');
        $redirect = Url::fromRoute('gumlet_sf.styles')->setAbsolute()->toString();
        $response = new RedirectResponse($redirect);
        $response->send();
        return;
    }
}
