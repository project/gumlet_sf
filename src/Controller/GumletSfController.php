<?php

namespace Drupal\gumlet_sf\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class GumletSf Controller.
 *
 * @package Drupal\gumlet_sf\Controller
 */
class GumletSfController extends ControllerBase
{
    /**
     * Display.
     *
     * @return string
     *   Return Listing Image styles.
     */
    public function content()
    {

        $header = array(
          'image_styles' => t('STYLE NAME'),
          'mode' => t('STYLE MODE'),
          'operations' => t('OPERATIONS'),
        );

        $rows = array();
        $gumletSF = \Drupal::database()->select('gumlet_sf', 'gs');
        $gumletSF->fields('gs');
        $gumletSF->orderBy('gs.id', 'DESC');
        $gumletSFResults =  $gumletSF->execute()->fetchAll();
        if (isset($gumletSFResults) && !empty($gumletSFResults)) {
            $gumletModes = gumlet_sf_modes();
            foreach ($gumletSFResults as $styles) {
                $styleObject['image_styles'] = $styles->name;
                $styleObject['mode'] = $gumletModes[$styles->mode];

                $confirmText = t('Are you sure to remove @style_name', array('@style_name' => $styles->name));
                $editUrl = Url::fromRoute('gumlet_sf.styles_edit', ['id' => $styles->id]);
                $editLink = Link::fromTextAndUrl(t('Edit'), $editUrl)->toString();
                $deleteUrl = Url::fromRoute('gumlet_sf.styles_delete', ['id' => $styles->id]);
                $deleteLink = Link::fromTextAndUrl(t('Remove'), $deleteUrl)->toString();
                $build_link_action = [
                  'action_edit' => [
                    '#type' => 'html_tag',
                    '#value' => $editLink,
                    '#tag' => 'div',
                    '#attributes' => ['class' => ['action-edit']]
                  ],
                  'action_delete' => [
                  '#type' => 'html_tag',
                  '#value' => $deleteLink,
                  '#tag' => 'div',
                  '#attributes' => ['class' => ['action-edit'], 'onclick' => "return confirm('" . $confirmText . "')"]
                  ]
                ];
                $styleObject['operations'] = \Drupal::service('renderer')->render($build_link_action);
                $rows[] = $styleObject;
            }
        }
        $build['table'] = [
          '#type' => 'table',
          '#header' => $header,
          '#rows' => $rows,
          '#empty' => t('There are currently no styles. <a href=":url">Add a new one</a>.', [':url' =>
          Url::fromRoute('gumlet_sf.styles_add')->toString()])
        ];
        return $build;
    }

    /**
     * {@inheritdoc}
     */
    public function delete($id = null)
    {
        $redirect = Url::fromRoute('gumlet_sf.styles')->setAbsolute()->toString();
        if (!$id) {
            \Drupal::messenger()->addMessage(t('Please select atleast one style.'), 'status');
            $redirect = Url::fromUri('gumlet_sf.styles');
            $response = new RedirectResponse($redirect);
            $response->send();
            return;
        }
        \Drupal::database()->delete('gumlet_sf')
        ->condition('id', $id)
        ->execute();
        \Drupal::messenger()->addMessage(t('Gumlet Image Styles has been deleted Successfully.'), 'status');
        $response = new RedirectResponse($redirect);
        $response->send();
        return;
    }
}
