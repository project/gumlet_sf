<?php

namespace Drupal\gumlet_sf\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;

/**
 * Plugin implementation of the 'image' formatter.
 *
 * @FieldFormatter(
 *   id = "gumlet_sf",
 *   label = @Translation("Gumlet SF Image"),
 *   field_types = {
 *     "image"
 *   },
 *   quickedit = {
 *     "editor" = "image"
 *   }
 * )
 */
class GumletSFImageFormatter extends GumletSFImageFormatterBase implements ContainerFactoryPluginInterface
{
    /**
     * The current user.
     *
     * @var \Drupal\Core\Session\AccountInterface
     */
    protected $currentUser;

    /**
     * Constructs an ImageFormatter object.
     *
     * @param string $plugin_id
     *   The plugin_id for the formatter.
     * @param mixed $plugin_definition
     *   The plugin implementation definition.
     * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
     *   The definition of the field to which the formatter is associated.
     * @param array $settings
     *   The formatter settings.
     * @param string $label
     *   The formatter label display setting.
     * @param string $view_mode
     *   The view mode.
     * @param array $third_party_settings
     *   Any third party settings settings.
     * @param \Drupal\Core\Session\AccountInterface $current_user
     *   The current user.
     */
    public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountInterface $current_user)
    {
        parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
        $this->currentUser = $current_user;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $plugin_id,
            $plugin_definition,
            $configuration['field_definition'],
            $configuration['settings'],
            $configuration['label'],
            $configuration['view_mode'],
            $configuration['third_party_settings'],
            $container->get('current_user'),
            $container->get('entity_type.manager')->getStorage('image_style')
        );
    }

    /**
     * {@inheritdoc}
     */
    public static function defaultSettings()
    {
        return [
          'gumlet_sf_style' => '',
          'image_link' => '',
        ] + parent::defaultSettings();
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $gumlet_sf = gumlet_sf_options();
        $description_link = Link::fromTextAndUrl(
            $this->t('Configure Image Styles'),
            Url::fromRoute('entity.image_style.collection')
        );
        $element['gumlet_sf_style'] = [
          '#title' => t('Gumlet Styles'),
          '#type' => 'select',
          '#default_value' => $this->getSetting('gumlet_sf_style'),
          '#empty_option' => t('None'),
          '#options' => $gumlet_sf
        ];
        $link_types = [
          'content' => t('Content'),
          'file' => t('File'),
        ];
        $element['image_link'] = [
          '#title' => t('Link image to'),
          '#type' => 'select',
          '#default_value' => $this->getSetting('image_link'),
          '#empty_option' => t('Nothing'),
          '#options' => $link_types,
        ];

        return $element;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary()
    {
        $summary = [];
        $gumlet_sf = gumlet_sf_options();

        // Styles could be lost because of enabled/disabled modules that defines
        // their styles in code.
        $gumlet_sf_style = $this->getSetting('gumlet_sf_style');
        if (isset($gumlet_sf_style) && !empty($gumlet_sf_style)) {
            $summary[] = t('Gumlet Image Style: @mode', ['@mode' => $gumlet_sf[$gumlet_sf_style]]);
        } else {
            $summary[] = t('Original image');
        }

        $link_types = [
          'content' => t('Linked to content'),
          'file' => t('Linked to file'),
        ];
        // Display this setting only if image is linked.
        $image_link_setting = $this->getSetting('image_link');
        if (isset($link_types[$image_link_setting])) {
            $summary[] = $link_types[$image_link_setting];
        }

        return $summary;
    }

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = [];
        $files = $this->getEntitiesToView($items, $langcode);

        // Early opt-out if the field is empty.
        if (empty($files)) {
            return $elements;
        }

        $url = "";
        $image_link_setting = $this->getSetting('image_link');
        // Check if the formatter involves a link.
        if ($image_link_setting == 'content') {
            $entity = $items->getEntity();
            if (!$entity->isNew()) {
                $url = $entity->toUrl();
            }
        } elseif ($image_link_setting == 'file') {
            $link_file = true;
        }


        $gumlet_sf_style_id = $this->getSetting('gumlet_sf_style');


        // Collect cache tags to be added for each item in the field.
        $base_cache_tags = [];

        foreach ($files as $delta => $file) {
            $cache_contexts = [];
            if (isset($link_file)) {
                $image_uri = $file->getFileUri();
                // @todo Wrap in file_url_transform_relative(). This is currently
                // impossible. As a work-around, we currently add the 'url.site' cache
                // context to ensure different file URLs are generated for different
                // sites in a multisite setup, including HTTP and HTTPS versions of the
                // same site. Fix in https://www.drupal.org/node/2646744.
                $url = \Drupal::service('file_url_generator')->generate($image_uri);
                $cache_contexts[] = 'url.site';
            }
            $cache_tags = Cache::mergeTags($base_cache_tags, $file->getCacheTags());

          // Extract field item attributes for the theme function, and unset them
          // from the $item so that the field template does not re-render them.
            $item = $file->_referringItem;
            $item_attributes = $item->_attributes;
            unset($item->_attributes);

            $elements[$delta] = [
              '#theme' => 'gumlet_sf_formatter',
              '#item' => $item,
              '#item_attributes' => $item_attributes,
              '#gumlet_sf_style_id' => $gumlet_sf_style_id,
              '#url' => $url,
              '#cache' => [
                'tags' => $cache_tags,
                'contexts' => $cache_contexts,
              ],
            ];
        }

        return $elements;
    }
}
