<?php

namespace Drupal\gumlet_sf\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'image_url' formatter.
 *
 * @FieldFormatter(
 *   id = "gumlet_sf_url",
 *   label = @Translation("Gumlet SF Url to image"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class GumletSFImageUrlFormatter extends GumletSFImageFormatter
{
    /**
     * {@inheritdoc}
     */
    public static function defaultSettings()
    {
        return [
          'gumlet_sf_style' => '',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $element = parent::settingsForm($form, $form_state);

        unset($element['image_link']);

        return $element;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary()
    {
        $summary = parent::settingsSummary();
        return [$summary[0]];
    }

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = [];

        /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
        if (empty($images = $this->getEntitiesToView($items, $langcode))) {
          // Early opt-out if the field is empty.
            return $elements;
        }
        $sf_data = [];
        $gumlet_sf_style_id = $this->getSetting('gumlet_sf_style');
        if (isset($gumlet_sf_style_id)) {
            $sf_data = gumlet_sf_load($gumlet_sf_style_id);
        }

        foreach ($images as $delta => $image) {
            $image_uri = $image->getFileUri();
            $url = \Drupal::service('file_url_generator')->generateAbsoluteString($image_uri);
            $url = \Drupal::service('file_url_generator')->transformRelative($url);
            if (isset($sf_data) && !empty($sf_data)) {
                $url .= gumlet_sf_formatter($sf_data);
            }
          // Add cacheability metadata from the image and image style.
            $cacheability = CacheableMetadata::createFromObject($image);
            if ($gumlet_sf_style_id) {
                $cacheability->addCacheableDependency(CacheableMetadata::createFromObject($gumlet_sf_style_id));
            }

            $elements[$delta] = ['#markup' => $url];
            $cacheability->applyTo($elements[$delta]);
        }
        return $elements;
    }
}
