CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended modules
* Installation
* Additional Requirements
* Configuration
* Usage

INTRODUCTION
------------

  Gumlet Image Styles And Formatters provide additional Image styles and Formatters to your drupal site, which serves the images with Gumlet using the help of the S3 File System. You can able create multiple image styles of the Gumlet.

  Gumlet does not store your original media. You must store original media in storage of your choice and give us read access to those storage systems.

  Gumlet is a complete image optimization solution. It takes care of all 5 aspects of optimized image delivery:

    1. Automatic Image Resize: All images are resized according to the user's device screen size. Every visitor of your site gets the right-sized images.
    2. Image compression: Gumlet uses perceptually lossless image compression to reduce the image file size without any effect on image quality. We even optimize GIFs and SVGs.
    3. Automatic WebP: Images are delivered in "new age" WebP format whenever the browser supports them. WebP images are 40% lighter than JPEG images for the same quality.
    4. Lazy load: The plug-in has an in-built lazy load to ensure the fastest page view experience.
    5. Caching and CDN Delivery: All images processed via Gumlet are delivered through a super-fast image CDN worldwide. It ensures the lowest latency for all of your images.


REQUIREMENTS
------------

This module requires the following modules:

* S3 File System (https://www.drupal.org/project/s3fs)

Additional Requirements
-----------------------

First, you need to create a Gumlet account here if you don't already have one. It is completely free. We recommend signing up using your business email id to make billing, account management, and support much easier.

Next up, log in to your account and go to the image sources page. Click the + Create New Source button. A source tells Gumlet how to fetch the original image.

Choose Cloud Storage from the list and click on the Next button. Please check out our advanced Configure Image Source guide if you have any confusion.

While setting up the source, you will create a Gumlet subdomain, which will be used to deliver your images. You may choose any available subdomain; it does not affect the performance or SEO

Amazon S3 (Cloud Storage)
---------

You can use images stored in the Amazon S3 bucket as an image source. Gumlet needs GetObject, ListBucket, and GetBucketLocation permissions to access the images. Please create an access token with these permissions and add them while creating the image store.

You can optionally specify the base path while creating Amazon S3 image source. If your image is stored at s3://yourbucket/some/image/path/lenna.jpg and if you specify /some/image/path as the base path, your resultant URL for Gumlet will become yourdomain.gumlet.com/lenna.jpg. Without a base path, the Gumlet URL will be yourdomain.gumlet.com/some/image/path/lenna.jpg


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) 
  for further information.

Configuration
------------
* Make sure you configured the "CNAME Settings" (Serve files from a custom domain by using an appropriately named bucket, e.g. "yourdomain.gumlet.com".
) and "Custom Host Settings" (Connect to an S3-compatible storage service other than Amazon.
) from S3 File system Configurations (/admin/config/media/s3fs)
* Go to admin > configurations.
* Find the menu with name of "Gumlet Styles".
* Here you will be able to create gumlet styles as per gumlet documentation: (https://docs.gumlet.com/reference/image-formats).



Usage
-------------

    1. Create Gumlet Styles (/admin/config/media/gumlet-sf).
    2. Create Image or Media field with storage of S3 Bucket.
    3. You will get an image formatter named "Gumlet SF Url to image" or "Gumlet SF Image" at the manage display of your entity.
    4. When you implement the formatter then you can able to Gumlet Image Styles. 
    5. You will be able to receive images with the help of Gumlet styles.